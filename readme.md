## Password Cracker 

A simple shell script to generate password using openssl and match with an existing hash. 


## Resource 

- Dictionary : https://github.com/danielmiessler/SecLists/tree/master/Passwords
- openssl passwd manual page: https://wiki.openssl.org/index.php/Manual:Passwd(1)
- openssl passwd actual source code: https://github.com/openssl/openssl/blob/master/apps/passwd.c

## Running

```
./cracker.sh 10_million_password_list_top_1000000.txt

```
