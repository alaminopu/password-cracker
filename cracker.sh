#!/bin/bash

filename="$1"
MPASS="\$1\$YXE8SSDm\$gxL1YEsRnwVzQDvBi6Rvg/"

while read -r line
do
	password="$line"
	echo "Read from file - $password"
	generated=$(openssl passwd -1 -salt YXE8SSDm $password)
	echo "$generated"
	echo ""

	if [ "$generated" = "$MPASS" ]
	then
		echo "Match found for - $password"
		break
	fi

done < "$filename" 

